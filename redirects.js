'use strict';

function redirectTo(url) {
  window.location = url;
}

switch (window.location.hostname) {
  case 'cal.stanke.cz':
  case 'calendar.stanke.cz':
    redirectTo('https://calendar.google.com/a/stanke.cz');
    break;
  case 'contacts.stanke.cz':
    redirectTo('https://contacts.google.com/');
    break;
  case 'docs.stanke.cz':
    redirectTo('https://docs.google.com/a/stanke.cz');
    break;
  case 'drive.stanke.cz':
    redirectTo('https://drive.google.com/a/stanke.cz');
    break;
  case 'mail.stanke.cz':
    redirectTo('https://mail.google.com/a/stanke.cz');
    break;
  default:
    redirectTo('https://www.stanke.cz/');
    break;
}
